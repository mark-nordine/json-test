{-# LANGUAGE OverloadedStrings, DeriveGeneric #-}

import Data.Aeson
import GHC.Generics

data Profile = Profile { age :: Int, isNice :: Bool } deriving (Show, Generic)

data User = User {
  id :: Int,
  name :: String,
  profile :: Profile
} deriving (Show, Generic)

instance FromJSON Profile
instance ToJSON Profile

instance FromJSON User
instance ToJSON User

main :: IO ()
main = do
  let myProfile = Profile 36 False
  let user = User 1 "Mark" myProfile
  print user

  -- encode will give you back ByteString

  let encodedJson = encode user
  print encodedJson

  -- decode will give you back Maybe

  let parsedUser = decode encodedJson :: Maybe User
  case parsedUser of
    Just newUser -> print newUser
    Nothing      -> putStrLn "No dice"
